public class CardDeck {
    public static void main(String[] args) {
        String[] CARD_SUITS = {"Clubs", "Diamonds", "Hearts", "Spades"};
        String[] CARD_RANKS = {"2", "3", "4", "5", "6", "7", "8", "9", "10",
            "J", "Q", "K", "A"};
        
        //initialize
        int noCards = CARD_SUITS.length * CARD_RANKS.length;
        
        String[] card_deck = new String[noCards];
        
        for(int i=0; i < CARD_RANKS.length; i++) {
            for(int j=0; j < CARD_SUITS.length; j++) {
                card_deck[i*CARD_SUITS.length + j] = 
                    CARD_RANKS[i] + " " + CARD_SUITS[j];
            }
        }
        
        //shuffle card deck
        for(int i=0; i < noCards; i++) {
            int random_card = i + (int) (Math.random() * (noCards - i));
            
            String temp_card = card_deck[random_card];
            card_deck[random_card] = card_deck[i];
            card_deck[i] = temp_card;
        }
        
        //print
        for(int i=0; i < noCards; i++) {
            System.out.println(card_deck[i]);
        }
    }
}